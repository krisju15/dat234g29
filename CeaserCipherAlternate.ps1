#Cipher program

$key = 0
$index = 0
$alphabet = ""
$alphabet = [char[]](65..90)

Write-host "Would you like to encrypt or decrypt? e/d"
$ed = read-host

if ($ed -eq "e") {

Write-host "Do you wish to encrypt using a specific key? y/n"
$yn = read-host

if ($yn -eq "n") {

Write-host "Enter text"
$text = read-host
clear

$textlength=$text.length
    While ($key -lt 26)
        {
        Write-host -NoNewLine $key `t`t
        $textlength=$text.length
            While ($textlength -gt 0)
            {
                $CurrentLetter = 0..($alphabet.count - 1) | Where {$alphabet[$_] -eq $text[$index]}
                #Moves letter in the alphabet list equivalent to key
				$NewLetter = $CurrentLetter + $key
                If ($NewLetter -gt 25)
                {$NewLetter = $NewLetter - 26}
                
                Write-Host -NoNewLine $alphabet[$NewLetter]
                    $textlength --
                     $index++
            }
                $index = 0
                $key ++
                Write-Host ""
        }
	}
	
elseif ($yn -eq "y") { 
    
	Write-Host "Please enter a key from 1-25"
	$key = read-host
	Write-host "Enter text"
	$text = read-host
	
        $textlength=$text.length
            While ($textlength -gt 0)
            {
                $CurrentLetter = 0..($alphabet.count - 1) | Where {$alphabet[$_] -eq $text[$index]}
                $NewLetter = $CurrentLetter + $key
                If ($NewLetter -gt 25)
                {$NewLetter = $NewLetter - 26}
                
                Write-Host -NoNewLine $alphabet[$NewLetter]
                    $textlength --
                     $index++
            }
		}
}

elseif ($ed -eq "d") {

Write-host "Do you wish to decrypt using a specific key? y/n"
$yn = read-host
if ($yn -eq "n") {
Write-host "Enter text"
$text = read-host
clear

$textlength=$text.length
    While ($key -lt 26)
        {
        Write-host -NoNewLine $key `t`t
        $textlength=$text.length
            While ($textlength -gt 0)
            {
                $CurrentLetter = 0..($alphabet.count + 1) | Where {$alphabet[$_] -eq $text[$index]}
                $NewLetter = $CurrentLetter + $key
                If ($NewLetter -gt 25)
                {$NewLetter = $NewLetter - 26}
                
                Write-Host -NoNewLine $alphabet[$NewLetter]
                    $textlength --
                     $index++
            }
                $index = 0
                $key ++
                Write-Host ""
        }
		}

elseif($yn -eq "y") {
    
	Write-Host "Please enter a key from 1-25"
	$key = read-host
	Write-host "Enter text"
	$text = read-host
	clear
		
        $textlength=$text.length
            While ($textlength -gt 0)
            {
                $CurrentLetter = 0..($alphabet.count + 1) | Where {$alphabet[$_] -eq $text[$index]}
                $NewLetter = $CurrentLetter + $key
                If ($NewLetter -gt 25)
                {$NewLetter = $NewLetter - 26}
                
                Write-Host -NoNewLine $alphabet[$NewLetter]
                    $textlength --
                     $index++
            }
		} 
		}

else {
Write-host "Restart and enter valid input"
pause
}
Write-Host ""
pause
