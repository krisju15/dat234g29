$range = 1..254

$range | ForEach-Object {

Get-WmiObject Win32_PingStatus -Filter "Address='192.168.0.$_' and Timeout=200 and ResolveAddressNames='true' and StatusCode=0" | select ProtocolAddress*
 
Write-Progress “Scanning Network” $address -PercentComplete (($_/$range.Count)*100)
}